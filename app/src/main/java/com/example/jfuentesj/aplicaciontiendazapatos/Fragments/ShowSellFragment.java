package com.example.jfuentesj.aplicaciontiendazapatos.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.example.jfuentesj.aplicaciontiendazapatos.Bases.BaseFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.R;

/**
 * Created by dbenitez on 3/14/2018.
 */

public class ShowSellFragment extends BaseFragment {


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public int getLayout() {
        return R.layout.fragment_show_sell;
    }
}
