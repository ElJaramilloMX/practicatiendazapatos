package com.example.jfuentesj.aplicaciontiendazapatos.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.jfuentesj.aplicaciontiendazapatos.Activities.SellerActivity;
import com.example.jfuentesj.aplicaciontiendazapatos.Adapters.AdapterRecyclerEditFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.Bases.BaseActivity;
import com.example.jfuentesj.aplicaciontiendazapatos.Bases.BaseFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.Models.Almacen;
import com.example.jfuentesj.aplicaciontiendazapatos.Models.TeniModel;
import com.example.jfuentesj.aplicaciontiendazapatos.R;

import java.util.ArrayList;
import java.util.List;


public class SellFragment extends BaseFragment implements AdapterRecyclerEditFragment.OnItemClicked{
AdapterRecyclerEditFragment adapterRecyclerEditFragment;
 List<TeniModel> list;
    RecyclerView recyclerView;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_sell);

        list = Almacen.lista;

        adapterRecyclerEditFragment = new AdapterRecyclerEditFragment(list,getContext(),this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapterRecyclerEditFragment);
        recyclerView.hasFixedSize();
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_sell;
    }

    @Override
    public void ItemClicked(TeniModel model) {
    }


    @Override
    public void ShowDialog(int position) {
      final int pos = position;
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Vender")
                    .setPositiveButton("Vender", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Almacen.lista.get(pos).setSelled(false);
                            adapterRecyclerEditFragment.notifyItemChanged(pos);
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.dismiss();
                        }
                    });
            // Create the AlertDialog object and return it
        builder.create();
        builder.show();

    }


}
