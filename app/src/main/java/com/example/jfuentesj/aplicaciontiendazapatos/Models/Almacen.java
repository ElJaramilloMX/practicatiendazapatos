package com.example.jfuentesj.aplicaciontiendazapatos.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lrodriguez on 3/15/2018.
 */

public class Almacen {
    private static final Almacen almacen = new Almacen();
    public static List<TeniModel> lista;

    public static Almacen getInstance() {
        return almacen;
    }

    private Almacen() {

        lista = initTeniList();
    }

    private List<TeniModel> initTeniList(){
        List<TeniModel> TeniList = new ArrayList<>();

        TeniList.add(new TeniModel("1","Bota","Puma","Rojo",25.0f,false,380.0f));
        TeniList.add(new TeniModel("2","Deportivo","Puma","Negro",25.0f,false,380.0f));
        TeniList.add(new TeniModel("3","Zapato","Puma","Marron",25.0f,false,380.0f));
        TeniList.add(new TeniModel("4","Bota","Puma","Azul",25.0f,false,380.0f));
        TeniList.add(new TeniModel("5","Deportivo","Puma","Blanco",25.0f,false,380.0f));
        TeniList.add(new TeniModel("6","Casual","Puma","Negro",25.0f,false,380.0f));
        TeniList.add(new TeniModel("7","Deportivo","Puma","Azul",25.0f,false,380.0f));
        TeniList.add(new TeniModel("8","Bota","Puma","Negro",25.0f,false,380.0f));


        return TeniList;
    }
}
