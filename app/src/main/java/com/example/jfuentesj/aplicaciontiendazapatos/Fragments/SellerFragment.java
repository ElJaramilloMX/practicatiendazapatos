package com.example.jfuentesj.aplicaciontiendazapatos.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jfuentesj.aplicaciontiendazapatos.Bases.BaseFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.R;

/**
 * Created by lrodriguez on 3/14/2018.
 */

public class SellerFragment extends BaseFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public int getLayout() {
        return 0;
    }
}
