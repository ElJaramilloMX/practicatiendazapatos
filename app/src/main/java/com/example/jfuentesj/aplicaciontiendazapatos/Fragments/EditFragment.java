package com.example.jfuentesj.aplicaciontiendazapatos.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jfuentesj.aplicaciontiendazapatos.Adapters.AdapterRecyclerEditFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.Bases.BaseFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.Models.TeniModel;
import com.example.jfuentesj.aplicaciontiendazapatos.R;


public class EditFragment extends BaseFragment implements AdapterRecyclerEditFragment.OnItemClicked{

    private AdapterRecyclerEditFragment adapterRecyclerEditFragment;

    public EditFragment() {
        // Required empty public constructor
    }

    public static EditFragment newInstance(){
        return new EditFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public int getLayout() {
        return R.layout.fragment_edit;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void ItemClicked(TeniModel model) {

    }

    @Override
    public void ShowDialog(int position) {

    }
}
