package com.example.jfuentesj.aplicaciontiendazapatos.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.example.jfuentesj.aplicaciontiendazapatos.Bases.BaseActivity;
import com.example.jfuentesj.aplicaciontiendazapatos.Bases.BaseFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.Fragments.AdminFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.Fragments.EditFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.Fragments.addFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.R;

/**
 * Created by lrodriguez on 3/14/2018.
 */

public class AdminActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bnmBarra.inflateMenu(R.menu.menu_admin);

        fragment = new addFragment();

        showFragment();


        bnmBarra.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int itemId = item.getItemId();

                switch (itemId){
                    case R.id.action_add:
                        fragment = new addFragment();
                        showFragment();
                        return true;

                    case R.id.action_edit:
                        fragment = new EditFragment();
                        showFragment();
                        return true;

                    default:
                        return true;
                }
            }
        });
    }

    @Override
    public Fragment getFragment() {
        return fragment;
    }

    @Override
    public void showFragment() {
        fragmentManager.beginTransaction()
                .replace(R.id.container, getFragment())
                .commit();
    }
}
