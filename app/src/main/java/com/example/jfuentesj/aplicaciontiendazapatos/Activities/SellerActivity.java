package com.example.jfuentesj.aplicaciontiendazapatos.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.example.jfuentesj.aplicaciontiendazapatos.Bases.BaseActivity;
import com.example.jfuentesj.aplicaciontiendazapatos.Fragments.SellFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.Fragments.SellerFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.Fragments.ShowSellFragment;
import com.example.jfuentesj.aplicaciontiendazapatos.R;

/**
 * Created by lrodriguez on 3/14/2018.
 */

public class SellerActivity extends BaseActivity {



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bnmBarra.inflateMenu(R.menu.menu_seller);


        fragment = new SellFragment();

        showFragment();

        bnmBarra.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int itemId = item.getItemId();

                switch (itemId){
                    case R.id.action_sell:
                        fragment = new SellFragment();
                        showFragment();
                        return true;
                    case R.id.action_show:
                        fragment = new ShowSellFragment();
                        showFragment();
                        return true;

                    default:
                        return true;
                }


            }
        });
    }

    @Override
    public void showFragment(){
        fragmentManager.beginTransaction()
                .replace(R.id.container, getFragment())
                .commit();
    }

    @Override
    public Fragment getFragment() {

        return fragment;
    }
}
