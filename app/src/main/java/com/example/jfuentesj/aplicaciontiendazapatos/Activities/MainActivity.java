package com.example.jfuentesj.aplicaciontiendazapatos.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jfuentesj.aplicaciontiendazapatos.Models.Almacen;
import com.example.jfuentesj.aplicaciontiendazapatos.Models.TeniModel;
import com.example.jfuentesj.aplicaciontiendazapatos.R;

import java.util.List;


public class MainActivity extends AppCompatActivity {
    EditText editTextUsuario;
    EditText editTextPass;
    Button btnIniciarSesion;
    String nameUsuario, passUsuario;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextUsuario = findViewById(R.id.et_usuario_login);
        editTextPass = findViewById(R.id.et_pass_login);
        btnIniciarSesion = findViewById(R.id.btn_iniciar_sesion);


        btnIniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateForm()) {
                    nameUsuario = editTextUsuario.getText().toString();
                    passUsuario = editTextPass.getText().toString();
                    if (nameUsuario.equals("admin") && passUsuario.equals("admin")) {
                        Intent intent = new Intent(MainActivity.this, AdminActivity.class);
                        startActivity(intent);
                    } else if (nameUsuario.equals("vendedor") && passUsuario.equals("soyrico")) {
                        Intent intent = new Intent(MainActivity.this, SellerActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity.this, "Usuario o Contraseña equivocado", Toast.LENGTH_SHORT).show();
                    }
                    editTextPass.setText("");
                    editTextUsuario.setText("");
                }
            }
        });
    }

    public Boolean validateForm() {
        Boolean itsValid = true;
        if (editTextUsuario.getText().toString().isEmpty()) {
            editTextUsuario.setError("Introduce Usuario");
            itsValid = false;
        }
        if (editTextPass.getText().toString().isEmpty()) {
            editTextPass.setError("Introduce Contraseña");
            itsValid = false;
        }

        return itsValid;
    }
}

