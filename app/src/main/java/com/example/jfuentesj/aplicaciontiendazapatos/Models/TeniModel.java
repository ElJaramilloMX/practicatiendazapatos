package com.example.jfuentesj.aplicaciontiendazapatos.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jfuentesj on 3/14/2018.
 */

public class TeniModel {

    public String teniID;
    public String type;
    public String mark;
    public String color;
    public float size;
    public boolean selled;
    public float price;
    public static List<TeniModel>  TeniList;

    public String getTeniID() {
        return teniID;
    }

    public void setTeniID(String teniID) {
        this.teniID = teniID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public boolean isSelled() {
        return selled;
    }

    public void setSelled(boolean selled) {
        this.selled = selled;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * Constructor con parametros para crear un objeto TeniModel
     * @param teniID identificador unico del teni
     * @param type tipo de teni
     * @param mark marca de teni
     * @param color color de teni
     * @param size tamaño de teni
     * @param selled indicador si el teni está vendido o no
     * @param price precio del teni
     */

    public TeniModel(String teniID, String type, String mark, String color, float size, boolean selled, float price){

        this.teniID = teniID;
        this.type = type;
        this.mark = mark;
        this.color = color;
        this.size = size;
        this.selled =selled;
        this.price = price;
    }



    public List<TeniModel> initTeniList(){
        TeniList = new ArrayList<>();
        TeniList.add(new TeniModel("1","Bota","Puma","Rojo",25.0f,false,380.0f));
        TeniList.add(new TeniModel("2","Deportivo","Puma","Negro",25.0f,false,380.0f));
        TeniList.add(new TeniModel("3","Zapato","Puma","Marron",25.0f,false,380.0f));
        TeniList.add(new TeniModel("4","Bota","Puma","Azul",25.0f,false,380.0f));
        TeniList.add(new TeniModel("5","Deportivo","Puma","Blanco",25.0f,false,380.0f));
        TeniList.add(new TeniModel("6","Casual","Puma","Negro",25.0f,false,380.0f));
        TeniList.add(new TeniModel("7","Deportivo","Puma","Azul",25.0f,false,380.0f));
        TeniList.add(new TeniModel("8","Bota","Puma","Negro",25.0f,false,380.0f));
        return TeniList;
    }



    /**
     *
     * @return TeniList Devuelve un array List con los zapatos disponibles
     */
    public List<TeniModel> getTeniList()
    {
        return TeniList;
    }

    /**
     * Método para agregagar un nuevo Zapato
     * @param newTeni Nuevo Zapato a ingresar
     */
    public void addTeni(TeniModel newTeni)
    {
        TeniList.add(newTeni);
    }

    /**
     * Método para indicar si un zapato ha sido vendido
     * @param teniID ID del teni a buscar para modificar su valor
     * @param selled valor boleano para indicar si un zapato está vendido
     */
    public void sellTeni(String teniID, boolean selled)
    {
        for (TeniModel teni : TeniList) {
            if(teni.getTeniID().equals(teniID))
            {
                teni.setSelled(selled);
                break;
            }
        }
    }
}
