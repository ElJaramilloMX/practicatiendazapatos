package com.example.jfuentesj.aplicaciontiendazapatos.Bases;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.BottomNavigationView;
import com.example.jfuentesj.aplicaciontiendazapatos.R;

/**
 * Created by lrodriguez on 3/14/2018.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected BottomNavigationView bnmBarra;
    protected FragmentManager fragmentManager;
    protected Fragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);


        fragmentManager = getSupportFragmentManager() ;
        bnmBarra = (BottomNavigationView) findViewById(R.id.bnmBarra);
    }

    public abstract Fragment getFragment();

    public abstract void showFragment();


}
