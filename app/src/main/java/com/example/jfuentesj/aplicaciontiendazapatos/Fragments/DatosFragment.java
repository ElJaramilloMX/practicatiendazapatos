package com.example.jfuentesj.aplicaciontiendazapatos.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.example.jfuentesj.aplicaciontiendazapatos.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DatosFragment extends Fragment {
    RadioGroup rgTipoZapato;
    EditText etMarca;
    EditText etColor;
    EditText etTamaño;
    EditText etPrecio;
    Button btnSaveChanges;


    public DatosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_datos, container, false);
    }

}
