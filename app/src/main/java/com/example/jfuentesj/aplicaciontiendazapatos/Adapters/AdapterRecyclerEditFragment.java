package com.example.jfuentesj.aplicaciontiendazapatos.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jfuentesj.aplicaciontiendazapatos.Models.TeniModel;
import com.example.jfuentesj.aplicaciontiendazapatos.R;

import java.util.List;



public class AdapterRecyclerEditFragment extends RecyclerView.Adapter<AdapterRecyclerEditFragment.ItemViewHolder> {


    private List<TeniModel> mList;
    private Context context;
    private OnItemClicked mListener;

    public AdapterRecyclerEditFragment(List<TeniModel> mList, Context context, OnItemClicked mListener) {
        this.mList = mList;
        this.context = context;
        this.mListener = mListener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shoes,parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        final TeniModel model = mList.get(position);
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mListener.ShowDialog(position);
                    return true;
                }

            });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.ItemClicked(model);
            }
        });
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView  tvType, tvMark,tvColor, tvSize, tvStatus, tvPrice;

        public ItemViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_icon_shoe);
            tvType = itemView.findViewById(R.id.tv_type_shoe);
            tvMark = itemView.findViewById(R.id.tv_mark_shoe);
            tvColor = itemView.findViewById(R.id.tv_color_shoe);
            tvSize = itemView.findViewById(R.id.tv_size_shoe);
            tvStatus = itemView.findViewById(R.id.tv_status_shoe);
            tvPrice = itemView.findViewById(R.id.tv_price_shoe);
        }
    }

    public interface OnItemClicked{
        void ItemClicked(TeniModel model);
        void ShowDialog (int position);
    }

}
